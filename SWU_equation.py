__author__ = 'toby'
from math import log


class SeparationWork:

    def __init__(self, assay_f, assay_p, assay_t):
        self.assay_f = assay_f
        self.assay_p = assay_p
        self.assay_t = assay_t

    @staticmethod
    def value_function(assay):
        log_input = (1 - assay) / assay
        return (1 - (2 * assay)) * log(log_input)

    def get_product(self, swu, feed, tail):
        norm_feed = feed * self.value_function(self.assay_f)
        norm_tail = tail * self.value_function(self.assay_t)
        return (norm_feed + swu - norm_tail) / self.value_function(self.assay_p)

    def get_feed(self, swu, product, tail):
        norm_product = product * self.value_function(self.assay_p)
        norm_tail = tail * self.value_function(self.assay_t)
        return (norm_product + norm_tail - swu) / self.value_function(self.assay_f)

    def get_tail(self, swu, feed, product):
        norm_feed = feed * self.value_function(self.assay_f)
        norm_product = product * self.value_function(self.assay_p)
        return (norm_feed + swu - norm_product) / self.value_function(self.assay_t)

    def product_from_feed(self, feed):
        return feed * ((self.assay_f - self.assay_t) / (self.assay_p - self.assay_t))

    def feed_from_product(self, product):
        return product * ((self.assay_p - self.assay_t) / (self.assay_f - self.assay_t))

    def product_from_tails(self, tails):
        return tails * ((self.assay_f - self.assay_t) / (self.assay_p - self.assay_f))

    def tails_from_product(self, product):
        return product * ((self.assay_p - self.assay_f) / (self.assay_f - self.assay_t))

    def swu_from_product(self, product):
        feed = self.feed_from_product(product)
        tails = self.tails_from_product(product)
        norm_product = product * self.value_function(self.assay_p)
        norm_feed = feed * self.value_function(self.assay_f)
        norm_tails = tails * self.value_function(self.assay_t)
        return norm_product + norm_tails - norm_feed
